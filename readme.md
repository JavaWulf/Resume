# Personal Resume

This repository contains my personal which was originally based off of a template found on <a href="html5up.net">HTML5 UP</a>. I plan on updating this website as my resume grows. The main reason that this is on GitLab is so that I can practice useing git version control, but if you have any comments, or would like to add anything to the site feel free to put in a PR or contact me.
The website is live at http://ansonbiggs.com and hosted on a https://www.digitalocean.com/ droplet.

The test.html is there just for examples of what is possible in the future and are left over from the original template.


## Credits:

### Template:
* html5up.net | @ajlkn

### Icons:
* Font Awesome (fontawesome.github.com/Font-Awesome)

### Other:
* jQuery (jquery.com)
* html5shiv.js (@afarkas @jdalton @jon_neal @rem)
* CSS3 Pie (css3pie.com)
* Respond.js (j.mp/respondjs)
* Skel (skel.io)
* formspree.io
